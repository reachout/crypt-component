/**
 * This file is part of Crypt-Decrypt service Component.
 * <p>
 * Crypt-Decrypt service Component is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * Crypt-Decrypt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with Crypt-Decrypt (file COPYING).  If not, see <https://www.gnu.org/licenses/>.
 * <p>
 * Copyright (C) 2019-2020 UShareSoft SAS, All rights reserved
 *
 * @author Alexandre Lefebvre
 */
package org.ow2.crypt;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.ow2.crypt.internal.CryptImpl;

/**
 * Tests for the {@link Crypt} component.
 * ⚠ Those test are integration tests that require the setup of specific environment:
 * - Create a folder /etc/ow2/wrappers
 * - Add the files provided in `src/test/resources` in the newly created folder
 */
public class ITCryptTest {

    private final CryptImpl crypt;

    {
        crypt = new CryptImpl();
    }

    @Test
    public void testEncryptDecrypt() {
        String input = "5&w#vXYky;HL.g<D";
        String encrypted = crypt.encrypt(input, null, null);
        String decrypted = crypt.decrypt(encrypted, null, null);

        assertEquals(input, decrypted);
    }

    @Test
    public void testEncryptDecryptOtherfile() {
        String input = "password";
        String encrypted = crypt.encrypt(input, "OTHER_KEYFILE", null);
        String decrypted = crypt.decrypt(encrypted, "OTHER_KEYFILE", null);

        assertEquals(input, decrypted);
    }

    @Test
    public void testEncryptDecryptWithkey() {
        String input = "password";
        String encrypted = crypt.encrypt(input, null, "ReachOut is an H2020 coordination & support action");
        String decrypted = crypt.decrypt(encrypted, null, "ReachOut is an H2020 coordination & support action");

        assertEquals(input, decrypted);
    }

}
