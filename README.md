# Crypt-Decrypt service component

XWiki component to encrypt and decrypt strings

## Summary
This simple component can be used to encrypt and decrypt strings.

There should be on the filesystem:
1. file /etc/ow2/wrappers/wrappers.conf containing a line 
```KEYFILE_LOCATION=path_to_file```
1. file path_to_file should also exist

## Documentation

A generated Javadoc can be found [here](https://reachout.ow2.io/crypt-component/). 

## License

 Crypt-Decrypt service Component is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Crypt-Decrypt is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Crypt-Decrypt (file COPYING).  If not, see <https://www.gnu.org/licenses/>.

 Copyright (C) 2019-2020 UShareSoft SAS, All rights reserved