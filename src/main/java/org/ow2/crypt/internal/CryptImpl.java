/**
 * This file is part of Crypt-Decrypt service Component.
 * <p>
 * Crypt-Decrypt service Component is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * Crypt-Decrypt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with Crypt-Decrypt (file COPYING).  If not, see <https://www.gnu.org/licenses/>.
 * <p>
 * Copyright (C) 2019-2020 UShareSoft SAS, All rights reserved
 *
 * @author Alexandre Lefebvre
 */
package org.ow2.crypt.internal;

import org.apache.commons.codec.digest.DigestUtils;
import org.ow2.crypt.Crypt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xwiki.component.annotation.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.Properties;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.Cipher;

import javax.inject.Singleton;

/**
 * Implementation of a <tt>Crypt</tt> component.
 */
@Component
@Singleton
public class CryptImpl implements Crypt {

    private String keyFileLocation;
    private Properties propertiesResource;
    private String md5;

    private static final Logger LOGGER = LoggerFactory.getLogger(CryptImpl.class);

    /**
     * There should be on the filesystem:
     * 1. file /etc/ow2/wrappers/wrappers.conf containing a line KEYFILE_LOCATION=path_to_file
     * 2. file path_to_file should also exist
     */
    public CryptImpl() {
        try {
            FileInputStream is = new FileInputStream("/etc/ow2/wrappers/wrappers.conf");
            propertiesResource = new Properties();
            propertiesResource.load(is);
            keyFileLocation = propertiesResource.getProperty("KEYFILE_LOCATION");
            FileInputStream keyfileio = new FileInputStream(keyFileLocation);
            md5 = DigestUtils.md5Hex(keyfileio);
        } catch (IOException e) {
            LOGGER.error("Error while instantiating CryptImpl", e);
        }
    }

    /**
     * Encrypt a String
     * @param input : the String to be encrypted
     * @param fileVariable: optional alternative variable in the configuration
     *                    file to use another file for computing the key
     * @param key: optional alternative key
     * @return the encrypted String
     */
    @Override
    public String encrypt(String input, String fileVariable, String key) {
        String md5Used;
        try {
            if (fileVariable != null) {
                FileInputStream is = new FileInputStream("/etc/ow2/wrappers/wrappers.conf");
                propertiesResource = new Properties();
                propertiesResource.load(is);
                keyFileLocation = propertiesResource.getProperty(fileVariable);
                FileInputStream keyfileio = new FileInputStream(keyFileLocation);
                md5Used = DigestUtils.md5Hex(keyfileio);
            } else if (key != null) {
                md5Used = DigestUtils.sha256Hex(key);
            } else {
                md5Used = md5;
            }
            LOGGER.debug("Key for encoding is: " + md5Used);
            byte[] decodedKey = Base64.getDecoder().decode(md5Used);
            Cipher cipher = Cipher.getInstance("AES");
            // rebuild key using SecretKeySpec
            SecretKey originalKey = new SecretKeySpec(Arrays.copyOf(decodedKey, 16), "AES");
            cipher.init(Cipher.ENCRYPT_MODE, originalKey);
            byte[] cipherText = cipher.doFinal(input.getBytes(StandardCharsets.UTF_8));
            return new String (Base64.getEncoder().encode(cipherText), StandardCharsets.ISO_8859_1);
        } catch (Exception e) {
            throw new RuntimeException(
                    "Error occurred while encrypting data", e);
        }
    }

    /**
     * Decrypt a String
     * @param input : the String to be decrypted
     * @param fileVariable: optional alternative variable in the configuration
     *                    file to use another file for computing the key
     * @param key: optional alternative key
     * @return the decrypted String
     */
    @Override
    public String decrypt(String input, String fileVariable, String key) {
        //System.out.println("Input for decoding is: " + input);
        String md5Used;
        try {
            if (fileVariable != null) {
                FileInputStream is = new FileInputStream("/etc/ow2/wrappers/wrappers.conf");
                propertiesResource = new Properties();
                propertiesResource.load(is);
                keyFileLocation = propertiesResource.getProperty(fileVariable);
                FileInputStream keyfileio = new FileInputStream(keyFileLocation);
                md5Used = DigestUtils.md5Hex(keyfileio);
            } else if (key != null) {
                md5Used = DigestUtils.sha256Hex(key);
            } else {
                md5Used = md5;
            }
            byte[] decodedKey = Base64.getDecoder().decode(md5Used);
            Cipher cipher = Cipher.getInstance("AES");
            // rebuild key using SecretKeySpec
            SecretKey originalKey = new SecretKeySpec(Arrays.copyOf(decodedKey, 16), "AES");
            cipher.init(Cipher.DECRYPT_MODE, originalKey);
            byte[] cipherText = cipher.doFinal(Base64.getDecoder().decode(input));
            return new String(cipherText);
        } catch (Exception e) {
            throw new RuntimeException(
                    "Error occurred while decrypting data", e);
        }
    }

}
