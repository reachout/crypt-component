/**
 This file is part of Crypt-Decrypt service Component.

 Crypt-Decrypt service Component is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Crypt-Decrypt is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Crypt-Decrypt (file COPYING).  If not, see <https://www.gnu.org/licenses/>.

 Copyright (C) 2019-2020 UShareSoft SAS, All rights reserved

 @author Alexandre Lefebvre
 */
package org.ow2.crypt.script;

import org.ow2.crypt.Crypt;
import org.ow2.crypt.internal.CryptImpl;
import org.xwiki.component.annotation.Component;
import org.xwiki.script.service.ScriptService;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;



/**
 * Crypt API made available as an XWiki service.
 */
@Component
@Named("crypt")
@Singleton
public class CryptService implements ScriptService
{
    @Inject
    private Crypt um;

    private void init() { um = new CryptImpl(); }

    /**
     * Encrypt a String
     * @param input : the String to be encrypted
     * @param fileVariable: optional alternative variable in the configuration
     *                    file to use another file for computing the key
     * @param key: optional alternative key
     * @return the encrypted String
     */
    public String encrypt(String input, String fileVariable, String key) {
        return this.um.encrypt(input, fileVariable, key);
    }

    /**
     * Decrypt a String
     * @param input : the String to be decrypted
     * @param fileVariable: optional alternative variable in the configuration
     *                    file to use another file for computing the key
     * @param key: optional alternative key
     * @return the decrypted String
     */
    public String decrypt(String input, String fileVariable, String key) {
        return this.um.decrypt(input, fileVariable, key);
    }

}
